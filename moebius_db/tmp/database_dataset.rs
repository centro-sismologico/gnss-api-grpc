#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct Uuid {
    #[prost(bytes = "vec", tag = "1")]
    pub value: ::prost::alloc::vec::Vec<u8>,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct GsofDataRpc {
    #[prost(float, tag = "1")]
    pub x: f32,
    #[prost(float, tag = "2")]
    pub y: f32,
    #[prost(float, tag = "3")]
    pub z: f32,
    /// vcv matrix
    #[prost(float, tag = "4")]
    pub vv_xx: f32,
    #[prost(float, tag = "5")]
    pub vv_yy: f32,
    #[prost(float, tag = "6")]
    pub vv_zz: f32,
    #[prost(float, tag = "7")]
    pub vv_xy: f32,
    #[prost(float, tag = "8")]
    pub vv_xz: f32,
    #[prost(float, tag = "9")]
    pub vv_yz: f32,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct TestDataRpc {}
/// github.com/protocolbuffers/protobuf/blob/main/src/google/protobuf/struct.proto
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DataReply {
    #[prost(message, optional, tag = "1")]
    pub id: ::core::option::Option<Uuid>,
    #[prost(message, optional, tag = "2")]
    pub dt_gen: ::core::option::Option<::prost_types::Timestamp>,
    #[prost(int32, tag = "3")]
    pub source_id: i32,
    #[prost(oneof = "data_reply::Data", tags = "4, 5")]
    pub data: ::core::option::Option<data_reply::Data>,
}
/// Nested message and enum types in `DataReply`.
pub mod data_reply {
    #[allow(clippy::derive_partial_eq_without_eq)]
    #[derive(Clone, PartialEq, ::prost::Oneof)]
    pub enum Data {
        #[prost(message, tag = "4")]
        Gsof(super::GsofDataRpc),
        #[prost(message, tag = "5")]
        Test(super::TestDataRpc),
    }
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct LastDataRequest {
    #[prost(int32, tag = "1")]
    pub source_id: i32,
    #[prost(int32, tag = "2")]
    pub amount: i32,
}
#[allow(clippy::derive_partial_eq_without_eq)]
#[derive(Clone, PartialEq, ::prost::Message)]
pub struct DeltaDataRequest {
    #[prost(int32, tag = "1")]
    pub source_id: i32,
    #[prost(message, optional, tag = "2")]
    pub dt_start: ::core::option::Option<::prost_types::Timestamp>,
    #[prost(message, optional, tag = "3")]
    pub dt_end: ::core::option::Option<::prost_types::Timestamp>,
}
